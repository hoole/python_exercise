from urllib import request
import re
# 断点调试
class Spider():
    url = 'https://www.panda.tv/cate/lol'
    root_pattern = '<div class="video-info">([\s\S]*?)</div>'
    root_name = '</i>([\s\S]*?)</span>'
    root_num = '<span class="video-number">([\s\S]*?)</span>'
    # 取内容
    def __fetch_content(self):
        r = request.urlopen(Spider.url)
        htmls = r.read()
        htmls = str(htmls,encoding='utf-8')
        return htmls
    # 分析内容
    def __anylize_(self,htmls):
        root_html = re.findall(Spider.root_pattern,htmls)
        infos = []
        for html in root_html:
            name = re.findall(Spider.root_name,html)
            num = re.findall(Spider.root_num,html)
            infoDict = {'name':name,'num':num}
            infos.append(infoDict)
        return infos
    # 处理内容
    def __refine(self,infos):
        l = lambda info:{
            'name':info['name'][0].strip(),
            'num':info['num'][0]
            }
        return map(l,infos)
    # 排序 
    def __sort(self,anchors):
        anchors = sorted(anchors,key=self.__key_name,reverse=True)
        return anchors
    # 排序key值
    def __key_name(self,anchor):
        r = re.findall('\d*',anchor['num'])
        num = float(r[0])
        if '万' in anchor['num']:
            num *= 10000
        return num
    # 打印显示
    def __show(self,anchors):  
        # for anchor in anchors:
        #     print(anchor['name'] + '---' + anchor['num'])
        for rank in range(0,len(anchors)):
            print('Rank:'+ str(rank+1) + '-----' + anchors[rank]['name'] + '-----' + anchors[rank]['num'])

    # 主入口
    def go(self):
        htmls = self.__fetch_content()
        infos = self.__anylize_(htmls)
        anchors = list(self.__refine(infos))
        anchors = self.__sort(anchors)
        self.__show(anchors)


f = Spider()

result = f.go()