from functools import reduce
def statistics(dic,k):
    if not k in dic:
        dic[k] = 1
    else:
        dic[k] += 1
    return dic

list1 = [1,2,3,1,2,3,4,5,2,3,8,5]
print(reduce(statistics,list1,{}))
        