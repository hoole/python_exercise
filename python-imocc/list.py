print(type([1,2,3,4,5]))
print(['1',2,3]+['3',1])
print([1,2]*3)
print([['1','2','3','4'],['1','2','3','4']])

print((1,2)+(2,3))
print(type((1)))
print(type(('1')))

print(type((1,)))
print('hello world'[0:8:2])
print('hello world'[1:2:3])


print({1,2,3,4,5,6} - {3,4})
print({1,2,3,4,5,6} & {3,4,7})
print({1,2,3,4,5,6} | {3,4,7})

print(type({1,2,3}))
print(type({1:1,2:2,3:3}))

print({(1,2):'新月打击'})