print (type(1))

print(type(1+1.0))

print(3/2)
print(3//2)
print(type(3/2))
print(type(3//2))

# 0b二进制 0o八进制 0x十六进制 bin()二进制转换 oct()八进制转换 int()十进制转换 hex()十六进制转换
print(0b10)
print(0o10)
print(0x1F)
print(bin(10))
print(bin(0o7))
print(int(0b111))
print(hex(888))
print(oct(0b101111))

print(
    '''
    hello world
    hello world
    hello world
    '''
)

print(r'hello\nworld')