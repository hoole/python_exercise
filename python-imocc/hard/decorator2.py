import time

def decorator(func):
    def wrapper(*args,**kw):
        print(time.time())
        func(*args,**kw)
    return  wrapper

@decorator
def f1(func_name):
    print('this is a function '+func_name)


@decorator
def f2(func_name,func2_name):
    print('this is a function '+func_name + func2_name)
# f = decorator(f1)

@decorator
def f3(func_name,func2_name,**kw):
    print(func_name)
    print(func2_name)
    print(kw)
# f()

f1('test name')
f2('test name','hahha')

f3('text1','text2',a=1,b=2,c='123')