# reduce
from functools import reduce

list_x = [1,2,3]

# 连续计算，连续调用lambda
r = reduce(lambda x,y: x+y,list_x)

print(r)

print(sum(list_x))


# (x,y)
# (0,0)
list_m = [(0,0),(1,3),(1,-1),(-2,8),(9,-3)]

sumX = 0
sumY = 0


def func(x,y):
    global sumX
    global sumY
    sumX += y[0]
    sumY += y[1]
    return sumX,sumY

r1 = reduce(func,list_m)

print(r1)
