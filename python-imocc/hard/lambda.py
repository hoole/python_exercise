# 匿名函数

def add(x,y):
    return x+y

f = lambda x,y: x+y

print(f(1,2))

# 三元表达式

# 条件为真是返回结果 if 条件判断 else条件为假返回结果
x = 3 
y = 2
r = x if x>y else y

print(r)

