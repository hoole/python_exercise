def demo(*param):
    print(param)
    print(type(param))

demo(1,2,3,4,5,6,7)

def demo1(param1,param2 = 2,*param3):
    print(param1)
    print(param2)
    print(param3)

demo1('a',1,2,3)

def demo2(param1,*param3,param2 = 2):
    print(param1)
    print(param3)
    print(param2)

demo2('a',1,2,3,param2='param')
