a = 1.12386
print(round(a,3))

def damage(skill1,skill2):
    damage1 = skill1 *3
    damage2 = skill2*2+10
    return damage1,damage2
damages = damage(3,6)
# 序列解包
skill1_damage,skill2_damage = damage(2,2)
print(damages)
print(skill1_damage,skill2_damage)

d = 1,2,3
print(d)

e,f,g = [4,5,6]
print(e)