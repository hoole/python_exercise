'''
    1.购买1级五行石：消耗金和砖石
    2.1级五行石合成3级五行石：消耗金、体力和1级五行石
    3.3级五行石合成4级五行石：消耗金、体力和1级五行石、一定概率
    4.4级五行石合成6级五行石：消耗金、体力和4级五行石
'''

'''
    购买一级石头
'''
l1_value = 0.75 #1颗1级石头消耗0.75金
l1_value_diamond = 8 # 1颗1级石头同时还需要消耗8颗砖石

'''
    1级合成3级
'''
l1_to_l3 = 12 # 1颗1级石头变成1颗3级石头，需要消耗12颗1级石头
l1_to_l3_gold = 0.39 # 同时还需要消耗0.39金
l1_to_l3_vit = 10 # 同时还需要消耗10点体力
'''
    3级合成4级
'''
l3_to_l4 = 16 # 1颗3级石头变成1颗4级石头，需要消耗16个1级石头
l3_to_l4_gold = 0.897 # 1颗3级石头变成1颗4级石头，需要消耗0.897金
l3_to_l4_vit = 10
l3_to_l4_rate = 0.4878 #1颗3级石头变成1颗4级石头，成功概率只有0.4878,
                        #如果失败，则金和16颗1级石头也将被扣除，但是不消耗体力
'''
    4级合成6级
'''
l4_to_l6 = 12 # 1颗4级石头变成6级石头，概率100%，需要消耗12颗4级石头
l4_to_16_gold = 19.75 # 需要消耗19.75金
l4_to_16_vit = 10
'''
已知1颗6级石头的市场售价为750金，请问是自己合成石头划算还是直接购买划算
其他数据：
    1颗砖石diamond 卖出0.05金
    1点体力vit 可以卖出1金
'''
# 4级合成6级所需材料
def consume_four(num=1):
    consume_stone_4 = 13*num
    consume_gold = 19.75
    consume_vit = 10
    return (consume_stone_4,consume_gold,consume_vit)
# 3级合成4级所需材料
def consume_three(num=1):
    consume_stone_3 = round((1*num)/0.4878,2)
    consume_stone_1 = round((num*16)/0.4878,2)
    consume_gold = round(0.897/0.4878,2)
    consume_vit = 10
    return (consume_stone_3,consume_stone_1,consume_gold,consume_vit)

#合成1个3级石头所需的材料
def consume_two(num = 1):
    consume_stone_1 = 13*num
    consume_gold = 0.39
    consume_vit = 10
    return (consume_stone_1,consume_gold,consume_vit)


consume_stone_4,consume_gold_4,consume_vit_4 = consume_four()
consume_stone_3,consume_stone_1_3,consume_gold_3,consume_vit_3 = consume_three(consume_stone_4)
consume_stone_1_2,consume_gold_2,consume_vit_2 = consume_two(consume_stone_3)

# 合成材料总共消耗的1级石头
consume_stone_1 = round(consume_stone_1_3 + consume_stone_1_2,2) 
# 合成材料总共消耗的金币
consume_gold = consume_gold_4+consume_gold_3+consume_gold_2
# 合成材料总共消耗的体力
consume_vit = consume_vit_4+consume_vit_3 + consume_vit_2

# 买1级石头消耗的金币
consume_stone_gold = round(consume_stone_1*l1_value,2)
# 消耗的砖石
consume_stone_masonry = round(l1_value_diamond*consume_stone_1,2)
# 总消耗的金币
consume_gold_total = consume_stone_gold + consume_gold

# 买6级石头所需金币
bug_gold = round(750 - consume_stone_masonry * 0.05 - consume_vit*1,2)
print(consume_gold_total,bug_gold)




