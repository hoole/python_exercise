a = (1,2,3,[1,2,3])
print(a[3][2])

a = (1,2,3,[1,2,4])
a[3][2] = '-4'
print(a)

print(2**5)

b = 1
b += b>=1
print(b)
print(1 + True)

print('fbc'<'bad')

print([3,3,5]>[2,1,4])

a = {1,2,3}
b = {2,1,3}
print(a == b)
print(a is b)
c = (1,2,3)
d = (2,1,3)
print(c==d)
print(c is d)

print (7 & 10)
print(7|10)