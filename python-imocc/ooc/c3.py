# 类的基本作用：封装
class Student():
    name = '晓假'
    sum1 = 0
    # 实例方法 
    # self就是当前调用方法的对象
    def __init__(self,name,age):
        # 构造函数
        self.name = name
        self.age = age
        # print(Student.sum1)
        print (self.__class__.sum1)
student1 = Student('小明',81)

# student2 = Student('小李',81)

# student3 = Student('小红',81)