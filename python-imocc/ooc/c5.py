# 类的基本作用：封装
class Student():
    name = '晓假'
    sum1 = 0
    # 实例方法 
    # self就是当前调用方法的对象
    def __init__(self,name,age):
        # 构造函数
        self.name = name
        self.age = age
        # Student.sum1 += 1
        # self.__class__.sum1 += 1
        # print (self.__class__.sum1)
        # print (Student.sum1)
    #类方法 
    @classmethod
    def plus_sum(cls):
        cls.sum1 += 1
        print(cls.sum1)
    # 静态方法
    @staticmethod
    def add(x,y):
        print('this is a static method')

    
student1 = Student('小明',81)
# Student.plus_sum()
# student2 = Student('小李',81)
# Student.plus_sum()
# student3 = Student('小红',81)
# Student.plus_sum()
student1.add(1,1)
Student.add(1,1)