# 类的基本作用：封装
class Student():
    name = '晓假'
    school = '东京小学'
    def __init__(self,name,age):
        self.name = name
        self.age = age
    def print_file(self):
        print('name:' + self.name)
        print('age:' + str(self.age))

student1 = Student('小明',81)
student2 = Student('小李',12)
print(student1.name)
# print(student2.name,student2.age)
# print(Student.school)
# student.print_file()