'''
    成员可见性：公开和私有
'''

# 类的基本作用：封装
class Student():
    name = '晓假'
    sum1 = 0
    # 实例方法 
    # self就是当前调用方法的对象
    def __init__(self,name,age):
        # 构造函数
        self.name = name
        self.age = age
        self.__score = 0
    def marking(self,score):
        if score < 0:
            return '不能够给别人打负分'
        self.__score = score
        print(self.__score)
    #类方法 
    @classmethod
    def plus_sum(cls):
        cls.sum1 += 1
        print(cls.sum1)
    # 静态方法
    @staticmethod
    def add(x,y):
        print('this is a static method')

    
student1 = Student('小明',81)
student1.marking(12)
student1._Student__score = 11
print(student1.__dict__)

student2 = Student('小李',16)

print(student2.__dict__)

