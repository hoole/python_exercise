# 生成器
# print 0 ~ 10000

n = [i for i in range(0,10001)]
# print (n)

# for i in n:
#     print (i)

# 普通函数
# def gen(max):
#     n = 0
#     while n <=max:
#         print(n)
#         n += 1

# gen(10)


def gener(max):
    n = 0
    while n <=max:
        n += 1
        yield n

g = gener(10000)

print(next(g))




