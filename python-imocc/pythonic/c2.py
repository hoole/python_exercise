# 列表推导式

# set,dict 也可以被推导


a = [1, 2, 3, 4, 5, 6, 7, 8]

# 列表推导式实现
b = [i**2 for i in a if i >= 5]

print(b)


# map filter 实现
c = map(lambda x : x*x,filter(lambda y : y >= 5,a))

print(list(c))
