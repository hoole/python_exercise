# 可迭代对象,迭代器

class BookCollection:
    def __init__(self):
        self.data = ['往事','只能','回味']
        self.cur = 0
    def __iter__(self):
        return self
    def __next__(self):
        if self.cur >= len(self.data):
            raise StopIteration()
        r = self.data[self.cur]
        self.cur += 1
        return r
        
import copy
books = BookCollection()

books_copy = copy.copy(books)

# 迭代器一次性
for book in books:
    print(book)

for book in books_copy:
    print(book)
    
