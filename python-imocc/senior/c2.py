# 枚举的比较



from enum import Enum

class VIP(Enum):
    YELLOW = 1
    GREEN = 2
    BLACK = 3
    RED = 4
# 枚举类型 、 枚举的名字 、 枚举的值
print(VIP.YELLOW.value)
print(VIP.YELLOW.name)

print(VIP['YELLOW']) 
# VIP.YELLOW = 6
# print(VIP.YELLOW)
for v in VIP:
    print(v)



# 可变 没有防止相同值的功能
{'yellow':1,'green':2}

class TypeDiamond():
    yellow = 1
    green = 2