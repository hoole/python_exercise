# 闭包

origin = 0

def f1(pos):
    def f2(step):
        # 闭包记录上一次调用的状态
        nonlocal pos
        new_pos = pos + step
        pos = new_pos
        return new_pos 
    return f2


f = f1(origin)

print(f(1))

print(f.__closure__[0].cell_contents)
print(f(2))

print(f.__closure__[0].cell_contents)

print(f(3))

print(f.__closure__[0].cell_contents)
