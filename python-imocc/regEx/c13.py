# group
import re

s = 'life is short, i use python,i love python'
# match
r = re.search('life(.*)python(.*)python',s)

print(r.group(0,1,2))

print(r.groups())

# group 获取分组的匹配

r1 = re.findall('life(.*)python(.*)python',s)

print(r1)