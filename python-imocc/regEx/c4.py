import re


a = 'python &1111___java\n67\r8phpsss'
# 贪婪
r = re.findall('[a-z]{3,6}',a)
print(r)


# 非贪婪

r1 = re.findall('[a-z]{3,6}?',a)

print(r1)


r2 = re.findall('[a-z]{3}',a)
print(r2)


