# json

import json

json_str = '{"name":"qiyue","age":18}'

# JSON object array
json_str1 = '[{"name":"qiyue","age":18},{"name":"qiyue","age":18}]'
student = json.loads(json_str)

# 反序列化
student1 = json.loads(json_str1)
print(type(student))
print(student)
print(student1)
# print(student['name'])
# print(student['age'])