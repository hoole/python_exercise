# 概括字符集

import re


a = 'python &1111___java\n67\r8php'

r = re.findall('[^0-9]',a)
print(r)

# \w 单词字符
r1 = re.findall('[A-Za-z0-9_]',a)
print(r1)

r2 = re.findall('\W',a)
print(r2)

# \s 空白字符 \S非空白字符

# .匹配除换行符\n之外的其他所有字符

