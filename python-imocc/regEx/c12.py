# match search
import re

s = 'ABC3721D86'
# match 首字母匹配
r = re.match('\d',s)
print(r)

# search 整个字符串匹配并返回第一个满足条件的字符串
r1 = re.search('\d',s)

print(r1.span())
print(r1.group())

r2 = re.findall('\d',s)
print(r2)

