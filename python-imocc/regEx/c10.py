# 匹配模式参数

import re

a = 'Pythonc#JavaPHPc#Javascript'

r = re.sub('c#','GO', a ,1)

r1 = a.replace('c#','GO')
print(r)
print(r1)

def convert(value):
    matched = value.group()
    return '!!' + matched + '!!'


r2 = re.sub('c#',convert, a)

print(r2)


