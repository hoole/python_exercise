import re

a = 'python &1111___javapythonnnnn\n67\r8phppytho'

# * 匹配0次或者无限多次
r = re.findall('python*',a)
print(r)

# + 匹配1次或者无限多次
r1 = re.findall('python+',a)
print(r1)

# ？匹配0次或者1次

r2 = re.findall('python?',a)
print(r2) 


