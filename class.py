class Person(object):

    def __init__(self, name, score):
        self.name = name
        self._score = score

    def get_grade(self):
        if self._score >= 90:
            return '优秀'
        elif self._score >= 60:
            return '及格'
        else:
            return '不及格'

p1 = Person('Bob', 90)
p2 = Person('Alice', 65)
p3 = Person('Tim', 48)

print (p1.get_grade())
print (p2.get_grade())
print (p3.get_grade())

class Person1(object):
    count = 0
    @classmethod
    def how_many(cls):
        return cls.count
    def __init__(self, name):
        self.name = name
        Person1.count = Person1.count + 1

print (Person1.how_many())
p1 = Person1('Bob')
print (Person1.how_many())
p2 = Person1('Job')
print (Person1.how_many())