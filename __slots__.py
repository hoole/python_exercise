# python 是动态语言，任何实例在运行期都可以动态的添加属性
# 使用python的一个特殊的__slots__来实现可以限制添加的属性
class Person(object):
    __slots__ = ('name', 'gender')

    def __init__(self, name, gender):
        self.name = name
        self.gender = gender


class Student(Person):
    __slots__ = ('score')

    def __init__(self, name,gender,score):
        super(Student, self).__init__(name, gender)
        self.score = score


s = Student('Bob', 'male', 59)
s.name = 'Tim'
s.score = 99
print(s.score)
