import os
print(os.path.isdir('C:\Windows'))

print(os.path.isfile('D:\SOFT\python-3.6.5.exe'))

class Person(object):
    pass

p1 = Person()
p1.name = 'Bart'

p2 = Person()
p2.name = 'Adam'

p3 = Person()
p3.name = 'Lisa'

L1 = [p1, p2, p3]
L2 = sorted(L1,key = lambda x:x.name)

print (L2[0].name)
print (L2[1].name)
print (L2[2].name)

class Person1(object):
    def __init__(self,name,sex,birth,**kw):
        self.name = name
        self.sex = sex
        self.birth = birth
        for k,v in kw.items():
            setattr(self,k,v)

xiaoming = Person1('Xiao Ming', 'Male', '1990-1-1', job='Student')

print (xiaoming.name)
print (xiaoming.job)
