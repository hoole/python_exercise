# python中编写带参数decorator
def log(prefix):
    def log_decorator(f):
        def wrapper(*args, **kw):
            print('[%s] %s()...' % (prefix, f.__name__))
            return f(*args, **kw)
        return wrapper
    return log_decorator


@log('DEBUG')
def test(x, y):
    return x+y


print(test(1, 2))


import time
from functools import reduce


def performance(unit):
    def pef_decorator(f):
        def wrapper(*args, **kw):
            t1 = time.time()
            r = f(*args, **kw)
            t2 = time.time()
            t = (t2-t1)*1000 if unit == 'ms' else (t2-t1)
            print(('call %s() in %f %s') % (f.__name__, t, unit))
            return r
        return wrapper
    return pef_decorator


@performance('ms')
def factorial(n):
    return reduce(lambda x, y: x*y, range(1, n+1))


print(factorial(10))
