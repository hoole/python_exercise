
# def fact(n):
#     if n == 1:
#         return 1
#     return n*fact(n-1)
# print(fact(5))
# 汉诺塔
# def move(n,a,b,c):
#     if n == 1:
#         print(a,'-->',c)
#         return
#     move(n-1,a,c,b)
#     print(a,'-->',c)
#     move(n-1,b,a,c)
# move (4,'A','B','C')

# def greet(x='world'):
#     return 'hello,'+x+'.'
# print(greet('jaja'))

# def format_name(s):
#     return s[0].upper() + s[1:].lower()
# print (list(map(format_name,['Adam','LISA','barT'])))

# from functools import reduce
# def prod(x, y):
#     return x*y

# print (reduce(prod, [2, 4, 5, 7, 12]))

# def cmp_ignore_case(s1, s2):
#     u1 = s1.upper()
#     u2 = s2.upper()
#     if u1 < u2:
#         return -1
#     if u1 > u2:
#         return 1
#     return 0


print(sorted(['bob', 'about', 'Zoo', 'Credit'], key=str.lower))

# def f():
# print ('call f()...')
# 定义函数g
# def g():
# print ('call g()...')
# 返回函数g
# return g

# 延迟计算
# def calc_sum(list):
#     def lazy_sum():
#         return sum(list)
#     return lazy_sum
# f = calc_sum([1,2,3,4])
# print(f)
# print(f())

# def g():
#     print('g()...')

# def h():
#     print('f()...')
#     return g

# 这个不是很懂，后面再看
def count():
    fs = []
    for i in range(1, 4):
        print(i)
        def f(j):
            def g():
                return j*j
            return g
        r = f(i)
        fs.append(r)
    return fs

f1, f2, f3 = count()
print(f1(), f2(), f3())
