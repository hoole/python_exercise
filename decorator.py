def log(f):
    def fn(*args,**kw):
        print('call ' + f.__name__ + '()...')
        return f(*args,**kw)
    return fn


from functools import reduce


@log
def factorial(n):
    return reduce(lambda x, y: x*y, range(1, n+1))


print(factorial(10))

@log
def add(x, y):
    return x+y

print (add(1, 2))

import time
# 打印函数调用的时间
def performance(f):
    def fn(*args,**kw):
        t1 = time.time()
        r = f(*args,**kw)
        t2 = time.time()
        print ('call %s() in %fs' %(f.__name__,(t2-t1)))
        return r
    return fn

@performance
def factorial1(n):
    return reduce(lambda x,y: x*y, range(1, n+1))

print (factorial1(10))
