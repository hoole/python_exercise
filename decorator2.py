# python 中完善decorator
import functools
def log(f):
    @functools.wraps(f)
    def wrapper(*args,**kw):
        print ('call...')
        return f(*args,**kw)
    return wrapper
@log
def f(x):
    pass
print(f.__name__)  